
import React from "react";
import LoginPage from "./pages/LoginPage";
import { useRouteMatch, Switch, Route } from "react-router-dom";

export default function Login(props) {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={match.url} component={LoginPage} />
    </Switch>
  );
}
