import { Container, makeStyles } from "@material-ui/core";
import React from "react";
import LoginForm from "../components/LoginForm";

const useStyles = makeStyles({
  root: {
    marginTop: 50,
  },
});

/* This is main page */
export default function LoginPage() {
  const classes = useStyles();
  return (
    <Container className={classes.root}>
      <LoginForm></LoginForm>
    </Container>
  );
}
