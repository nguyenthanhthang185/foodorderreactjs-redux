import React from "react";
import {
  Button,
  TextField,
  Grid,
  Paper,
  Typography,
  Box,
} from "@material-ui/core";
import * as commonAPI from "../../../common/handleAPI";
import { AuthContext } from "../../../contexts/AuthenticationContext";
import { Redirect } from "react-router-dom";
import { useForm } from "react-hook-form";

export default function LoginForm(props) {
  const { login, dispatch } = React.useContext(AuthContext);
  const { register, handleSubmit, errors } = useForm();
  const notify = React.useRef(null);

  const submitForm = async (data) => {
    const result = await commonAPI.fetchAPI(
      commonAPI.POST_LOGIN_API.URL,
      commonAPI.POST_LOGIN_API.method,
      data
    );
    if (result === "Successful") {
      dispatch({ type: "LOGGED" });
    } else {
      notify.current.innerHTML = result;
    }
  };

  return (
    <>
      {login && <Redirect to="/"></Redirect>}
      <Box>
        <Grid container spacing={0} justify="center" alignItems="center">
          <Grid item>
            <Grid container direction="column" justify="center" spacing={2}>
              <Paper variant="elevation" elevation={2}>
                <Grid item>
                  <Typography component="h1" variant="h3" align="center">
                    Sign in
                  </Typography>
                </Grid>
                <Grid item>
                  <form onSubmit={handleSubmit(submitForm)}>
                    <Grid container direction="column" spacing={2}>
                      <Grid item>
                        <TextField
                          type="text"
                          placeholder="Username"
                          fullWidth
                          name="username"
                          variant="outlined"
                          autoFocus
                          inputRef={register({
                            required: true,
                          })}
                        ></TextField>

                      </Grid>
                      <Grid item>
                        <TextField
                          type="password"
                          placeholder="Password"
                          fullWidth
                          name="password"
                          variant="outlined"
                          inputRef={register({
                            required: true,
                          })}
                        ></TextField>
                      </Grid>
                      <Grid item>
                        <Typography
                          ref={notify}
                          variant="h5"
                          component="h1"
                          color="error"
                        >{(errors.password || errors.username) && "Username and password is require"}</Typography>
                      </Grid>

                      <Grid item>
                        <Button
                          variant="contained"
                          color="primary"
                          type="submit"
                        >
                          Submit
                        </Button>
                      </Grid>
                    </Grid>
                  </form>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
