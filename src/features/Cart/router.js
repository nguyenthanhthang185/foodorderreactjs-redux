import { useRouteMatch, Switch } from "react-router-dom";
import React from "react";
import PrivateRoute from "../../routes/PrivateRoute";
import CartPage from "./pages/CartPage";

export default function Home(props) {
  const match = useRouteMatch();

  return (
    <Switch>
      <PrivateRoute path={match.url} component={CartPage} exact />
    </Switch>
  );
}
