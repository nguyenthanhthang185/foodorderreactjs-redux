import React from "react";
import { makeStyles } from "@material-ui/core";
import {CartContext} from '../../../contexts/CartContext';
import {Container} from '@material-ui/core';
import FoodCard from '../../../components/FoodCard';
import CheckOut from '../components/CheckOut';

const useStyles = makeStyles({
  root: {
    width: 800,
    height: 100,
    marginLeft: 200,
  },
});

/* This is main page */
export default function CartPage(props) {
  const { cart } = React.useContext(CartContext);
  const classes = useStyles();
  let total = 0;
  return (
    <>
      <Container className={classes.root}>
        {Object.keys(cart).map((element) => {
          total += cart[element].quantity * cart[element].price;
          return (
            <FoodCard
              key={element}
              item={cart[element]}
              option="cart"
            ></FoodCard>
          );
        })}
        <CheckOut total={total}></CheckOut>
      </Container>
    </>
  );
}
