import React from "react";
import {
  Typography,
  Card,
  CardContent,
  CardActions,
  makeStyles,
  Button,
} from "@material-ui/core";
import { CartContext } from "../../../contexts/CartContext";
import { Link } from "react-router-dom";
import { getCurrentDateTime } from "../../../common/handleOther";
import * as common from '../../../common/handleAPI';
import Order from "../../../models/orderModel";

const useStyles = makeStyles({
  card: {
    background: "#FFF0F5",
    color: "black",
    width: 300,
    marginTop: 10
  },
  button: {
    background: "#FFD700",
  },
});


export default function CheckOut(props) {
  const classes = useStyles();
  const { dispatch, cart } = React.useContext(CartContext);

  const checkout = () => {
    let currentCourse = 0;
    const dateTime = getCurrentDateTime();
    let total = 0;
    const orders = [];
    var keys = Object.keys(cart);
    keys.sort();
    for (const [index, element] of keys.entries()) {
      if (index !== 0 && cart[element].courseId !== currentCourse) {
        orders[orders.length - 1].totalOrder = total;
      }
      if (cart[element].courseId !== currentCourse) {
        currentCourse = cart[element].courseId;
        total = cart[element].price * cart[element].quantity;
        orders.push(new Order(currentCourse, dateTime));
      } else {
        total += cart[element].price * cart[element].quantity;
      }
      orders[orders.length - 1].addFoodItem({
        food_id: cart[element].food_id,
        quantity: cart[element].quantity,
      });
    }
    orders[orders.length - 1].totalOrder = total;
    common.fetchAPI(common.POST_ORDER_API.URL, common.POST_ORDER_API.method, orders)
    dispatch({ type: "CLEAR_FOOD" });
    alert("Successful");
  };
  return (
    <>
      {props.total > 0 ? (
        <Card className={classes.card} variant="outlined">
          <CardContent>
            <Typography className={classes.pos} variant="h6">
              <b>Total: </b>
              {props.total}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              className={classes.button}
              variant="outlined"
              onClick={checkout}
            >
              Checkout
            </Button>
          </CardActions>
        </Card>
      ) : (
        <Button className={classes.button} variant="outlined">
          <Link to="/">Go to course</Link>
        </Button>
      )}
    </>
  );
}
