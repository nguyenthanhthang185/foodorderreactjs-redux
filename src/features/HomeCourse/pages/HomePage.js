import React from "react";

import * as commonAPI from "../../../common/handleAPI";

import { Container,  Typography } from "@material-ui/core";
import HomeCourseList from "../components/HomeCourseList";

// const useStyles = makeStyles({
//   grid: {
//     margin: 5,
//   },
// });

/* This is main page */
export default function HomePage(props) {
  const [course, setCourse] = React.useState({
    listCourse: [],
    isLoading: true,
  });
  const getCourse = async () => {
    const result = await commonAPI.fetchAPI(
      commonAPI.GET_HOME_API.URL,
      commonAPI.GET_HOME_API.method
    );
    setCourse({ listCourse: result, isLoading: false });
  };

  React.useEffect(() => {
    getCourse();
  }, [course.isLoading]);

  return (
    <Container>
      {course.isLoading ? (
        "Loading..."
      ) : course.listCourse.length !== 0 ? (
        <HomeCourseList listCourse={course.listCourse}></HomeCourseList>
      ) : (
        <Typography color="secondary">No Course</Typography>
      )}
    </Container>
  );
}
