import React from "react";
import * as commonAPI from "../../../common/handleAPI";
import { useParams } from "react-router-dom";
import TabMenu from "../components/TabMenu";
const MenuContext = React.createContext();

function MenuPage() {
  const [restaurantMenu, setMenu] = React.useState({
    menu: [],
    isLoading: true,
  });
  let { courseId } = useParams();
  let { restaurantId } = useParams();
  restaurantId = parseInt(restaurantId);
  courseId = parseInt(courseId);

  React.useEffect(() => {
    const getMenu = async () => {
      try {
        const result = await commonAPI.fetchAPI(
          commonAPI.GET_MENU_API.URL + restaurantId,
          commonAPI.GET_MENU_API.method
        );
        setMenu({ menu: result, isLoading: false });
      } catch (error) {
        console.error(error);
      }
    };
    getMenu();
  }, [restaurantId]);

  return (
    <>
      <MenuContext.Provider
        value={{ restaurantId: restaurantId, courseId: courseId }}
      >
        {restaurantMenu.isLoading ? (
          "Loading..."
        ) : restaurantMenu.menu.length !== 0 ? (
          <TabMenu menu={restaurantMenu.menu}></TabMenu>
        ) : (
          "No Menu"
        )}
      </MenuContext.Provider>
    </>
  );
}

export { MenuPage, MenuContext };
