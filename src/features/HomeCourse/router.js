import { useRouteMatch, Switch } from "react-router-dom";
import React from "react";
import PrivateRoute from "../../routes/PrivateRoute";
import HomePage from "./pages/HomePage";
import {MenuPage} from "./pages/MenuPage";

export default function Home(props) {
  const match = useRouteMatch();

  return (
    
    <Switch>
      <PrivateRoute path={match.url} component={HomePage} exact />
      <PrivateRoute
        path={`${match.url}menu/:courseId/:restaurantId`}
        component={MenuPage}
      />
    </Switch>
  );
}
