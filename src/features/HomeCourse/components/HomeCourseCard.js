import React from "react";
import CourseForm from "../../../components/CourseForm";
import {
  Card,
  CardContent,
  CardActions,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Link, useRouteMatch } from "react-router-dom";

const useStyles = makeStyles({
  card: {
    background: "linear-gradient(to right, #ffafbd, #ffc3a0)",
    color: "black",
  },
  button: {
    background: "#FFD700"
  }
});

export default function HomeCourseCard(props) {
  const classes = useStyles();
  const { url } = useRouteMatch();
  return (
    <Card className={classes.card} variant="outlined">
      <CardContent>
        <CourseForm info={props.course} option="home"></CourseForm>
      </CardContent>
      <CardActions>
        <Button className={classes.button} variant="contained" fullWidth>
          <Link to={`${url}menu/${props.course.course_id}/${props.course.restaurant_id}`}>JOIN</Link>
        </Button>        
      </CardActions>
    </Card>
  );
}

