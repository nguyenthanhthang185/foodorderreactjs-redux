import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import MainMenu from "./MainMenu";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  appBar: {
    background: '#F08080'
  }
}));

export default function TabMenu(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Tabs value={value} onChange={handleChange}>
          {props.menu.map((element, index) => {
            return <Tab label={element.category_name} key={index}></Tab>;
          })}
        </Tabs>
      </AppBar>
      {props.menu.map((element, index) => {
        return <MainMenu value={value} index={index} key={index} foods={element.foods}></MainMenu>
      })}
    </div>
  );
}
