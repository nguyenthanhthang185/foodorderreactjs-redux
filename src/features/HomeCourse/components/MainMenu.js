import React from "react";
import {
  Grid,
  makeStyles,
} from "@material-ui/core";
import FoodCard from "../../../components/FoodCard";

const useStyles = makeStyles({
  grid: {
    margin: 5,
  },
});

export default function MainMenu(props) {
  const { value, index } = props;
  const classes = useStyles();
  return (
    <div hidden={value !== index}>
      {value === index && (
        <Grid className={classes.grid} container spacing={3}>
          {props.foods.map((element) => {
            return (
              <Grid key={element.food_id} item xs={3}>
                <FoodCard item={element}></FoodCard>
              </Grid>
            );
          })}
        </Grid>
      )}
    </div>
  );
}
