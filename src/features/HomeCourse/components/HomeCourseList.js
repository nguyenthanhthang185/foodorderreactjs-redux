import React from "react";
import HomeCourseCard from "../components/HomeCourseCard";
import { Grid, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  grid: {
    margin: 5,
  },
});

export default function HomeCourseList(props) {
  const classes = useStyles();
  return (
    <Grid className={classes.grid} container spacing={3}>
      {props.listCourse.map((element) => {
        return (
          <Grid key={element.course_id} item xs={4}>
            <HomeCourseCard course={element}></HomeCourseCard>
          </Grid>
        );
      })}
    </Grid>
  );
}
