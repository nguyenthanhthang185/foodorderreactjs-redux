import React from "react";
import * as commonAPI from "../../../common/handleAPI";
import { Typography, Container } from "@material-ui/core";
import MyCourseList from "../components/MyCourseList";
import AddCourseDialog from "../components/AddCourseDialog";

/* This is main page */
export default function MyCoursePage(props) {
  const [course, setCourse] = React.useState({
    listCourse: [],
    isLoading: true,
  });

  const getCourses = async () => {
    try {
      const result = await commonAPI.fetchAPI(
        commonAPI.GET_MY_COURSE_API.URL,
        commonAPI.GET_MY_COURSE_API.method
      );
      setCourse({ listCourse: result, isLoading: false });
    } catch (error) {
      console.error(error);
    }
  };

  React.useEffect(() => {
    getCourses();
  }, [course.isLoading]);

  return (
    <Container>
      <AddCourseDialog loading={loading => setCourse({isLoading: loading})} />
      {course.isLoading ? (
        "Loading..."
      ) : course.listCourse.length !== 0 ? (
        <MyCourseList listCourse={course.listCourse} />
      ) : (
        <Typography color="secondary">No Course</Typography>
      )}
    </Container>
  );
}
