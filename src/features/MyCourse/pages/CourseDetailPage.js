import React from "react";
import MainOrder from "../../../components/MainOrder";

import * as commonAPI from "../../../common/handleAPI";
import { useParams } from "react-router-dom";
import StatusCourse from "../../../components/StatusCourse";
import { Box } from "@material-ui/core";

export default function CourseDetailPage() {
  const [orders, setOrders] = React.useState({order: [], isLoading: true});
  const { courseId } = useParams();
  const id = parseInt(courseId);
  
  React.useEffect(() => {
    async function getOrders() {
      try {
        const result = await commonAPI.fetchAPI(
          commonAPI.GET_DETAIL_COURSE_API.URL + id,
          commonAPI.GET_DETAIL_COURSE_API.method
        );
        setOrders({order: result, isLoading: false});
      } catch (error) {
        console.error(error)
      }
    }
    getOrders();
  }, [id]);

  return (
    <>
      <Box display="flex" marginBottom={5} bgcolor="#F8F8FF">
        <Box m="auto">
          <StatusCourse courseId={id} status=""></StatusCourse>
        </Box>
      </Box>

      {orders.isLoading
        ? "Loading..."
        : orders.order.length !== 0
        ? orders.order.map((element, index) => {
            return (
              <MainOrder
                key={index}
                groupOrders={element}
                option="detail"
              ></MainOrder>
            );
          })
        : "No order"}
    </>
  );
}


