import React from "react";
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Select,
  MenuItem,
  InputLabel,
  Typography,
} from "@material-ui/core";
import * as commonAPI from "../../../common/handleAPI";
import { getCurrentDateTime } from "../../../common/handleOther";
import Course from "../../../models/courseModel";
import { useForm, Controller } from "react-hook-form";

export default function AddCourseDialog(props) {
  const [open, setOpen] = React.useState(false);
  const [restaurant, setRestaurant] = React.useState([]);
  const { register, handleSubmit, errors, control } = useForm();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // const handleChange = (e) => {
  //   setCourseInfo({ ...courseInfo, [e.target.name]: e.target.value });
  // };

  const addCourse = async (data) => {
    const date = getCurrentDateTime();
    const course = new Course(data.name, data.description, data.id, date);
    const result = await commonAPI.fetchAPI(
      commonAPI.POST_MY_COURSE_API.URL,
      commonAPI.POST_MY_COURSE_API.method,
      course
    );
    console.log(result);
    handleClose();
    props.loading(true);
  };

  const addRestaurant = async () => {
    const result = await commonAPI.fetchAPI(
      commonAPI.GET_LIST_RESTAURANT_API.URL,
      commonAPI.GET_LIST_RESTAURANT_API.method
    );
    setRestaurant(result);
  };

  React.useEffect(() => {
    addRestaurant();
  }, []);

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        <b>+</b>
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add Course</DialogTitle>
        <form onSubmit={handleSubmit(addCourse)}>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              label="Course Name"
              type="text"
              name="name"
              inputRef={register({
                required: true,
              })}
              fullWidth
            />
            <TextField
              margin="dense"
              label="Course Description"
              name="description"
              type="text"
              inputRef={register({
                required: true,
              })}
              fullWidth
            />
            <InputLabel>Restaurant</InputLabel>
            <Controller
              control={control}
              name="id"
              defaultValue=""
              rules={{ required: true }}
              as={
                <Select>
                  {restaurant.length !== 0 &&
                    restaurant.map((element) => {
                      return (
                        <MenuItem
                          key={element.restaurant_id}
                          value={element.restaurant_id}
                        >
                          {element.restaurant_name}
                        </MenuItem>
                      );
                    })}
                </Select>
              }
            />

            <Typography variant="h5" component="h1" color="error">
              {(errors.name || errors.description || errors.id) &&
                "Name, description and restaurant is require"}
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button color="primary" type="submit">
              Confirm
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}
