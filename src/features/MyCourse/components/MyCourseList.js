import React from "react";

import { Grid, makeStyles } from "@material-ui/core";
import MyCourseCard from "./MyCourseCard";

const useStyles = makeStyles({
  grid: {
    margin: 5,
  },
});

export default function MyCourseList(props) {
  const classes = useStyles();
  return (
    <Grid className={classes.grid} container spacing={3}>
      {props.listCourse.map((element) => {
        return (
          <Grid key={element.course_id} item xs={4}>
            <MyCourseCard course={element}></MyCourseCard>
          </Grid>
        );
      })}
    </Grid>
  );
}
