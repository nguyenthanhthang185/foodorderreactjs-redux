import React from "react";

import { Card, CardContent, CardActions, Button, makeStyles } from "@material-ui/core";
import { Link, useRouteMatch } from "react-router-dom";
import StatusCourse from '../../../components/StatusCourse';
import CourseItem from "../../../components/CourseForm";

const useStyles = makeStyles({
  card: {
    background: "linear-gradient(to right, #8e9eab, #eef2f3)",
    color: "black",
  },
  button: {
    background: "#228B22",
    width: 150,
    height: 50,
  },
});

export default function MyCourseCard(props) {
  const classes = useStyles()
  const {url } = useRouteMatch();
  return  (
      <Card className={classes.card} variant="outlined">
        <CardContent>
          <CourseItem info={props.course} option="my"></CourseItem>
        </CardContent>
        <CardActions>
          <Button className={classes.button} variant="contained">
            <Link to={`${url}/${props.course.course_id}`}>DETAIL</Link>
          </Button>
          <StatusCourse courseId={props.course.course_id} status={props.course.status}></StatusCourse>
        </CardActions>
      </Card>
  ) 
}

