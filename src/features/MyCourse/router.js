import { useRouteMatch, Switch } from "react-router-dom";
import React from "react";
import PrivateRoute from "../../routes/PrivateRoute";
import MyCoursePage from "./pages/MyCoursePage";
import CourseDetailPage from "./pages/CourseDetailPage";

export default function MyCourse(props) {
  const match = useRouteMatch();
  console.log(match);
  return (
    <Switch>
      <PrivateRoute path={match.url} component={MyCoursePage} exact />
      <PrivateRoute
        path={`${match.url}/my-course/:courseId`}
        component={CourseDetailPage}
      />
    </Switch>
  );
}
