import React from "react";
import MainOrder from "../../../components/MainOrder";
import * as commonAPI from "../../../common/handleAPI";

/* This is main page */
export default function MyOrderPage(props) {
  const [courseOrders, setCourseOrders] = React.useState({orders: [], isLoading: true});
  
  const getOrders = async () => {
    const result = await commonAPI.fetchAPI(
      commonAPI.GET_LIST_ORDER_API.URL,
      commonAPI.GET_LIST_ORDER_API.method
    );
    setCourseOrders({orders: result, isLoading: false});  
  };
  React.useEffect(() => {
    getOrders();
  }, []);

  return (
    <>
      {courseOrders.isLoading ? "Loading..." : (
        courseOrders.orders.length !== 0 ? (
            courseOrders.orders.map((element, index) => {
              return <MainOrder key={index} course={element} option="my" ></MainOrder>
            })
        ) : "No order"
      )}
    </>
  );
}

