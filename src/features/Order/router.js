import { useRouteMatch, Switch } from "react-router-dom";
import React from "react";
import MyOrderPage from "./pages/MyOrderPage";
import PrivateRoute from "../../routes/PrivateRoute";

export default function MyOrder(props) {
  const match = useRouteMatch();

  return (
    <Switch>
      <PrivateRoute path={match.url} component={MyOrderPage} />
    </Switch>
  );
}
