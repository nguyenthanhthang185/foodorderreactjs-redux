
const URL_API = "http://localhost:3000/api";
const GET_LOGIN_API = {
  method: "GET",
  URL: URL_API + "/auth/login",
};
const POST_LOGIN_API = {
  method: "POST",
  URL: URL_API + "/auth/login",
};
const GET_LOGOUT_API = {
    "method": "GET",
    "URL": URL_API + "/auth/logout"
}
// const POST_REGISTER_API = {
//     "method": "POST",
//     "URL": URL_API + "/auth/register"
// }
const GET_HOME_API = {
  method: "GET",
  URL: URL_API + "/home",
};
const GET_MY_COURSE_API = {
  method: "GET",
  URL: URL_API + "/my-courses",
};
const POST_MY_COURSE_API = {
    "method": "POST",
    "URL": URL_API + "/my-courses"
}
const GET_LIST_ORDER_API = {
    "method": "GET",
    "URL": URL_API + "/list-order"
}
const GET_LIST_RESTAURANT_API = {
    "method": "GET",
    "URL": URL_API + "/restaurant"
}

const GET_DETAIL_COURSE_API = {
    "method": "GET",
    "URL": URL_API + "/my-courses/details/"
}
const UPDATE_STATUS_ORDER_API = {
    "method": "PUT",
    "URL": URL_API + "/list-order?"
}
const UPDATE_STATUS_COURSE_API = {
    "method": "PUT",
    "URL": URL_API + "/my-courses?"
}
const GET_STATUS_COURSE_API = {
    "method": "GET",
    "URL": URL_API + "/my-courses/status/"
}
const GET_MENU_API = {
    "method": "GET",
    "URL": URL_API + "/menu/"
}

const POST_ORDER_API = {
  "method": "POST",
  "URL": URL_API + "/list-order"
}
async function fetchAPI(URL, method, body) {
  try {
    let response;
    if (method === "GET" || method === "PUT") {
      response = await fetch(URL, {
        method: method,
        credentials: "include",
      });
    } else if (method === "POST" && body) {
      response = await fetch(URL, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: method,
        body: JSON.stringify(body),
        credentials: "include",
      });
    }
    const contentType = response.headers.get("content-type");
    if (contentType && contentType.indexOf("application/json") !== -1) {
      return response.json();
    } else {
      return response.text();
    }
  } catch (error) {
    console.error(error);
  }
}



export {
  fetchAPI,
  GET_LOGIN_API,
  POST_LOGIN_API,
  GET_HOME_API,
  GET_MY_COURSE_API,
  GET_LIST_RESTAURANT_API,
  POST_MY_COURSE_API,
  GET_LIST_ORDER_API,
  GET_DETAIL_COURSE_API,
  GET_STATUS_COURSE_API,
  UPDATE_STATUS_COURSE_API,
  UPDATE_STATUS_ORDER_API,
  GET_MENU_API,
  POST_ORDER_API,
  GET_LOGOUT_API
};
