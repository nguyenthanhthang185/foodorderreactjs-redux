import React from "react";
import { authReducer } from "../reducers/authReducer";
import * as commonAPI from "../common/handleAPI";

const AuthContext = React.createContext();
const AuthenticationProvider = (props) => {
  const [login, dispatch] = React.useReducer(authReducer, false);
  const [firstLoading, setFirstLoading] = React.useState(true);
  React.useEffect(() => {
    async function getLogin() {
      try {
        const result = await commonAPI.fetchAPI(
          commonAPI.GET_LOGIN_API.URL,
          commonAPI.GET_LOGIN_API.method
        );
        if (result === "Logged on") {
          dispatch({ type: "LOGGED" });
        } else {
          dispatch({ type: "LOGOUT" });
        }
        setFirstLoading(false)
      } catch (error) {
        console.error(error);
      }
    }
    getLogin();
  }, []);

  return (
    <>
      {firstLoading ? (
        "Loading..."
      ) : (
        <AuthContext.Provider value={{ login, dispatch }}>
          {props.children}
        </AuthContext.Provider>
      )}
    </>
  );
};

export { AuthContext, AuthenticationProvider };
