import React from "react";
import { cartReducer } from "../reducers/cartReducer";
const CartContext = React.createContext();

const CartContextProvider = (props) => {

  const [cart, dispatch] = React.useReducer(cartReducer, [], () => {
    const localData = localStorage.getItem("cart");
    return localData ? JSON.parse(localData) : [];
  });
  
  React.useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  return (
    <CartContext.Provider value={{ cart, dispatch }}>
      {props.children}
    </CartContext.Provider>
  );
};

export { CartContextProvider, CartContext };
