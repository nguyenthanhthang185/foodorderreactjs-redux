import * as type from '../common/dispatchType';

export const cartReducer = (state, action) => {
  const newState = { ...state };
  switch (action.type) {
    case type.ADD_FOOD:
      if (newState[action.key] === undefined) {
        newState[action.key] = action.data;
      } else {
        newState[action.key].quantity += action.data.quantity;
      }
      return newState;
    case type.ADD_ONE_FOOD:
      newState[action.key].quantity += 1;
      return newState;
    case type.REMOVE_FOOD:
      newState[action.key].quantity -= 1;
      if (newState[action.key].quantity === 0) {
        delete newState[action.key];
      }
      
      return newState;
    case type.CLEAR_FOOD:
      return [];
    default:
      return newState;
  }
};
