import * as type from '../common/dispatchType';

export const authReducer = (state, action) => {
    switch (action.type) {
      case type.LOGGED:
        return true
      case type.LOGOUT:
        return false
      default:
        return state
    }
  };
  