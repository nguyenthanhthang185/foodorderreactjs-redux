export default class Course {
    constructor(name, description, restaurantId, date) {
      this.course_name = name
      this.course_description = description;
      this.restaurant_id = restaurantId
      this.created_date = date
      this.status = "Opening"
    }
  }
