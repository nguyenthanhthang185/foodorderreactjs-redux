export default class Order {
    constructor(courseId, orderDate) {
      this.course_id = courseId;
      this.ordered_date = orderDate;
      this.status = "Unpaid";
      this.total = 0;
      this.cart = "Order";
      this.foodItems = [];
    }
    set totalOrder(total) {
      this.total = total;
    }
  
    addFoodItem(item) {
      this.foodItems.push(item);
    }
  }