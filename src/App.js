import React, { Suspense } from "react";
import "./App.css";
import { Route, Switch } from "react-router-dom";

import { CartContextProvider } from "./contexts/CartContext";
import Header from "./components/Header";
import { AuthenticationProvider } from "./contexts/AuthenticationContext";
import PrivateRoute from "./routes/PrivateRoute";

const Home = React.lazy(() => import("./features/HomeCourse/router"));
const Login = React.lazy(() => import("./features/Login/router"));
const MyCourse = React.lazy(() => import("./features/MyCourse/router"));
const MyOrder = React.lazy(() => import("./features/Order/router"));
const Cart = React.lazy(() => import("./features/Cart/router"));

function App() {
  return (
    <>
      <Suspense fallback={<div>Loading...</div>}>
        <AuthenticationProvider>
          <CartContextProvider>
            <Header />
            <Switch>
              <Route path="/login" component={Login} exact />
              <PrivateRoute path="/" component={Home} exact />
              <PrivateRoute path="/my-course" component={MyCourse} exact />
              <PrivateRoute path="/my-order" component={MyOrder} />
              <PrivateRoute path="/cart" component={Cart} />
            </Switch>
          </CartContextProvider>
        </AuthenticationProvider>
      </Suspense>
    </>
  );
}

export default App;
