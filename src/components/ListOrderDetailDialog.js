import React from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@material-ui/core";

export default function ListOrderDetailDialog(props) {
  const [open, setOpen] = React.useState(true);
  const handleClose = () => {
    props.show(false)
    setOpen(false);
  };
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle><b>Order Detail</b></DialogTitle>
      <DialogContent>
        <List>
          {props.orderDetails.map((element, index) => {
            return (
              <ListItem divider key={index}>
                <ListItemText>
                  <Typography component="h2">
                    <b>Name: </b>
                    {element.food_name}
                  </Typography>
                  <Typography component="h2">
                    <b>Quantity: </b>
                    {element.quantity}
                  </Typography>
                  <Typography component="h2">
                    <b>Price: </b>
                    {element.price}
                  </Typography>
                </ListItemText>
              </ListItem>
            );
          })}
        </List>
      </DialogContent>
    </Dialog>
  );
}
