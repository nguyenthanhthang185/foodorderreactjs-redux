import React from "react";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import { Button, IconButton, makeStyles, Typography } from "@material-ui/core";
import { MenuContext } from "../features/HomeCourse/pages/MenuPage";
import { CartContext } from "../contexts/CartContext";
const useStyles = makeStyles({
  button: {
    background: "#228B22",
  },
});

/* Add item to cart component 
Use in menu and cart page
*/
export default function AddItemToCart(props) {
  const classes = useStyles();
  const id = React.useContext(MenuContext);
  const { dispatch } = React.useContext(CartContext);

  let courseId;
  let foodId = props.food.food_id;
  let initCount;
  let restaurantId
  if (id !== undefined) {
    courseId = id.courseId;
    restaurantId = id.restaurantId
    initCount = 0;
  } else {
    restaurantId = props.food.restaurantId
    courseId = props.food.courseId;
    initCount = props.food.quantity;
  }
  const [count, setCount] = React.useState(initCount);
  const key = "key" + courseId + foodId;

  const countUp = () => {
    const up = count + 1;
    setCount(up);
    if(id === undefined) {
      addOne()
    }
  };
  const countDown = () => {
    const down = count - 1;
    down >= 0 ? setCount(down) : setCount(0);
    if(id === undefined && down >= 0) {
      dispatch({ type: "REMOVE_FOOD", key: key })
    }
  };

  const addToCart = () => {
    dispatch({
      type: "ADD_FOOD",
      key: key,
      data: {
        courseId: courseId,
        restaurantId: restaurantId,
        food_id: props.food.food_id,
        food_name: props.food.food_name,
        price: props.food.price,
        image: props.food.image,
        quantity: count,
      },
    });
  };

  const addOne = () => {
    dispatch({ type: "ADD_ONE_FOOD", key: key })
  }
  return (
    <>
      <IconButton onClick={countDown}>
        <RemoveCircleOutlineIcon />
      </IconButton>
      <Typography component="p">{count}</Typography>
      <IconButton onClick={countUp}>
        <AddCircleOutlineIcon />
      </IconButton>
      {id === undefined ? null : (
        <Button
          disabled={count === 0}
          variant="outlined"
          className={classes.button}
          onClick={addToCart}
        >
          Add to cart
        </Button>
      )}
    </>
  );
}
