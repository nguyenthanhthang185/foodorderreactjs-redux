import React from "react";
import {
  Typography,
  Card,
  CardContent,
  CardActions,
  makeStyles,
  CardMedia,
  Button,
} from "@material-ui/core";
import AddItemToCart from "./AddItemToCart";
import { Link } from "react-router-dom";
const useStyles = makeStyles({
  text: {
    marginLeft: 20,
  },
  card: {
    width: "auto",
    height: "auto",
    background: "#F0F8FF",
    color: "black",
    marginBottom: 10,
  },
  media: {
    height: 150,
    width: 150,
    marginBottom: 10,
  },
  content: {
    display: "flex",
    flexWrap: "nowrap",
  },
  button: {
    background: "#FFD700",
    marginTop: 10,
    marginLeft: 20
  },
});

/* Display food info
Use in menu and cart page
*/

export default function FoodCard(props) {
  const isCart = props.option === "cart";
  //console.log(props);
  const classes = useStyles();
  return (
    <>
      <Card className={classes.card} variant="outlined">
        <CardContent className={classes.content}>
          <CardMedia
            className={classes.media}
            image={props.item.image === null ? "null" : props.item.image}
          ></CardMedia>
          <div>
            <Typography className={classes.text} variant="h6">
              <b>{props.item.food_name}</b>
            </Typography>
            {isCart ? (
              <Typography className={classes.text} variant="h6">
                <b>Total: </b>
                {props.item.price * props.item.quantity}
              </Typography>
            ) : (
              <Typography className={classes.text} variant="h6">
                <b>Price: </b>
                {props.item.price}
              </Typography>
            )}
            {isCart && (
              <Button className={classes.button} variant="contained" fullWidth>
                <Link
                  to={`/menu/${props.item.courseId}/${props.item.restaurantId}`}
                >
                  Go to course
                </Link>
              </Button>
            )}
          </div>
        </CardContent>
        <CardActions>
          <AddItemToCart food={props.item}></AddItemToCart>
        </CardActions>
      </Card>
    </>
  );
}
