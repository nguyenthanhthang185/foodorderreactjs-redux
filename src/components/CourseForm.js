import React from "react";
import { Typography, Divider, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  pos: {
    marginBottom: 10,
  },
});
/*Course item : MyCourse or Home
Option is "home" or "mycourse"
 */

export default function CourseForm(props) {
  const classes = useStyles();
  const dateTime = new Date(props.info.created_date).toLocaleString();
  return (
    <>
      <Typography align="center" className={classes.pos} variant="h4">
        <b>{props.info.course_name} </b>
      </Typography>
      <Divider />
      <Typography className={classes.pos} variant="h6">
        <b>Shop: </b>
        {props.info.restaurant_name}
      </Typography>
      <Typography className={classes.pos} variant="h6">
        <b>Description: </b>
        {props.info.course_description}
      </Typography>
      <Typography className={classes.pos} variant="h6">
        <b>Phone: </b>
        {props.info.restaurant_phone}
      </Typography>
      <Typography className={classes.pos} variant="h6">
        <b>Created Date: </b>
        {dateTime}
      </Typography>
      {props.option === "home" && (
        <Typography variant="h6">
          <b>Host: </b>
          {props.info.host}
        </Typography>
      )}
    </>
  );
}
