import React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  makeStyles,
  IconButton,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { CartContext } from "../contexts/CartContext";
import { AuthContext } from "../contexts/AuthenticationContext";
import * as common from "../common/handleAPI";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: 50,
    background: "#FFFFE0",
  },
  button: {
    margin: 10,
  },
  title: {
    flexGrow: 1,
    color: "#00FF00",
  },
}));

export default function Header() {
  const classes = useStyles();
  const { cart } = React.useContext(CartContext);
  const { login } = React.useContext(AuthContext);

  let count = 0;
  Object.keys(cart).forEach((element) => {
    count += cart[element].quantity;
  });

  const logout = () => {
    common
      .fetchAPI(common.GET_LOGOUT_API.URL, common.GET_LOGOUT_API.method)
      .then((result) => {
        console.log(result);
        window.location.replace("http://localhost:5500/login");
      });
  };

  return (
    <div>
      {login && (
        <AppBar className={classes.root} position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              FoodOrder
            </Typography>
            <Button className={classes.button}>
              <Link to="/">Home</Link>
            </Button>
            <Button className={classes.button}>
              <Link to="/my-order">My Order</Link>
            </Button>
            <Button className={classes.button}>
              <Link to="/my-course">My Course</Link>
            </Button>
            <Button className={classes.button} onClick={logout}>
              <Link to="#">Logout</Link>
            </Button>
            <IconButton>
              <Link to="/cart">
                <ShoppingCartIcon />
              </Link>
              <Typography component="p">{count}</Typography>
            </IconButton>
          </Toolbar>
        </AppBar>
      )}
    </div>
  );
}
