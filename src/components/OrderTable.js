import React from "react";
import * as commonAPI from "../common/handleAPI";

import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Select,
  MenuItem,
  Typography,
  makeStyles,
} from "@material-ui/core";
import ListOrderDetailDialog from "./ListOrderDetailDialog";
const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
}));

export default function OrderTable(props) {
  const classes = useStyles();
  const [showDialog, setShowDialog] = React.useState(false);
  const [status, setStatus] = React.useState(props.order.status); //Paid or Unpaid
  const option = props.option === "my"; //my order or course detail

  const showListDetail = (isShow) => {
    setShowDialog(isShow);
  };

  const dateTime = new Date(props.order.ordered_date).toLocaleString();
  const updateOrderStatus = (status) => {
    const url =
      commonAPI.UPDATE_STATUS_ORDER_API.URL +
      "order_id=" +
      props.order.order_id +
      "&status=" +
      status;
    commonAPI
      .fetchAPI(url, commonAPI.UPDATE_STATUS_ORDER_API.method)
      .then((result) => console.log(result))
      .catch((err) => console.error(err));
  };
  return (
    <>
      <Table className={classes.table} aria-label="simple table">
        <TableBody onClick={() => option && showListDetail(!showDialog)}>
          <TableRow>
            <TableCell>
              <b>Order date: </b>
              {dateTime}
            </TableCell>
            <TableCell align="center">
              <b>Total: </b>
              {props.order.total}
            </TableCell>
            <TableCell align="center">
              {option ? (
                <Typography component="p">
                  <b>Status: </b>
                  {props.order.status}
                </Typography>
              ) : (
                <Select
                  value={status}
                  displayEmpty
                  onChange={(e) => {
                    setStatus(e.target.value);
                    updateOrderStatus(status);
                  }}
                >
                  <MenuItem value="Paid">Paid</MenuItem>
                  <MenuItem value="Unpaid">Unpaid</MenuItem>
                </Select>
              )}
            </TableCell>
            {!option && (
              <TableCell align="right">
                <Button
                  onClick={() => {
                    showListDetail(!showDialog);
                  }}
                  variant="contained"
                  color="secondary"
                >
                  View Details
                </Button>
              </TableCell>
            )}
          </TableRow>
        </TableBody>
      </Table>
      {showDialog && (
        <ListOrderDetailDialog
          orderDetails={props.order.foodItems}
          show={showListDetail}
        ></ListOrderDetailDialog>
      )}
    </>
  );
}
