import React from "react";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  TableContainer,
  Paper,
  makeStyles
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import OrderTable from "./OrderTable";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    marginLeft: 500,
  },
  table: {
    minWidth: 650,
  },
  accordion: {
    background: "LightSalmon",
    marginBottom: 10
  },
  textColor: {
    color: "#228B22",
  },
}));

export default function MainOrder(props) {
  const classes = useStyles();
  const option = props.option === "my"; //my order or course detail : "my" or "detail"
  const data = option ? props.course : props.groupOrders;
  return (
    <div className={classes.root}>
      <Accordion className={classes.accordion}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          {option ? (
            <>
              <Typography>
                <b>Course: </b> {data.course_name}
              </Typography>
              <Typography className={classes.textColor}>
                ({data.status})
              </Typography>
              <Typography className={classes.heading}>
                <b>Host: </b>
                {data.host}
              </Typography>
            </>
          ) : (
            <>
              <Typography>
                <b>Order by: </b> {data.orderby}
              </Typography>
              <Typography className={classes.heading}>
                <b>Total: </b> {data.total}
              </Typography>
            </>
          )}
        </AccordionSummary>
        <AccordionDetails>
          <TableContainer component={Paper}>
            {data.orders.length === 0
              ? "No Order"
              : data.orders.map((element, index) => {
                  return (
                    <OrderTable
                      key={index}
                      order={element}
                      option={props.option}
                    ></OrderTable>
                  );
                })}
          </TableContainer>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
