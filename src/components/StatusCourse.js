import React from "react";
import { Select, MenuItem } from "@material-ui/core";
import * as commonAPI from "../common/handleAPI";

/*Use in my course and course detail */
export default function StatusCourse(props) {
  //Khi status được truyền vào thì sẽ ko gọi API để get status: Opening, Delivered, End of order, End of payment
  const [status, setStatus] = React.useState(props.status);
  const [isLoading, setLoading] = React.useState(true);

  React.useEffect(() => {
    async function getStatus() {
      const result = await commonAPI.fetchAPI(
        
        commonAPI.GET_STATUS_COURSE_API.URL + props.courseId,
        commonAPI.GET_STATUS_COURSE_API.method
      );
      setStatus(result[0].status);
    }
    
    if (props.status === "" && isLoading) {
      getStatus();
    }
    setLoading(false);
  }, [isLoading]);

  const updateCourseStatus = (status) => {
    let numberStatus = 0;
    switch (status) {
      case "Opening":
        numberStatus = 1;
        break;
      case "End of order":
        numberStatus = 2;
        break;
      case "Delivered":
        numberStatus = 3;
        break;
      case "End of payment":
        numberStatus = 4;
        break;
    }

    const url =
      commonAPI.UPDATE_STATUS_COURSE_API.URL +
      "course_id=" +
      props.courseId +
      "&status=" +
      numberStatus;
    commonAPI
      .fetchAPI(url, commonAPI.UPDATE_STATUS_COURSE_API.method)
      .then((result) => console.log(result))
      .catch((err) => console.error(err));
  };

  return (
    <>
      {isLoading ? (
        "Loading..."
      ) : (
        <Select
          variant="outlined"
          value={status}
          displayEmpty
          onChange={(e) => {
            setStatus(e.target.value);
            updateCourseStatus(e.target.value);
          }}
        >
          <MenuItem value="Opening">Opening</MenuItem>
          <MenuItem value="End of order">End of order</MenuItem>
          <MenuItem value="Delivered">Delivered</MenuItem>
          <MenuItem value="End of payment">End of payment</MenuItem>
        </Select>
      )}
    </>
  );
}
