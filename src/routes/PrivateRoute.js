import React from "react";
import { Redirect, Route } from "react-router-dom";
import {AuthContext} from "../contexts/AuthenticationContext";
function PrivateRoute({ component: Component, ...rest }) {
  const { login } = React.useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={(props) =>
        login ? (
          <Component {...props}></Component>
        ) : (
          <Redirect to="/login"></Redirect>
        )
      }
    />
  );
}

export default PrivateRoute;
